$(function() {

    
// La mise en forme du reste. A commenter si vous voulez la mise en forme originale

// /*

$("body").css({
    'height' : '100%',
    'margin' : '0',
    'background' : '#ffffff',
    'font-family' : 'sans-serif',
    'font-weight' : '100'
});

$("table").css({
   'width' : '800px',
   'border-collapse' : 'collapse',
   'overflow' : 'hidden',
   'box-shadow' : '0 0 20px rgba(0,0,0,0.1)'
});

$("container").css({
   'position' : 'absolute',
   'top' : '50%',
   'left' : '50%',
   'transform' : 'translate(-50%, -50%)'
});


$("th").css({
     'padding' : '15px',
   'background-color' : 'rgba(255,255,255,0.2)',
   'color' : '#000',
   'text-align' : 'left'
});

$("td").css({
   'padding' : '15px',
   'color' : '#000'
});

// */


// Les couleurs des cours

$("td:contains('Elec.')").css("background-color", "Red");

$("td:contains('BdD')").css("background-color", "Yellow");

$("td:contains('ProgS')").css("background-color", "LimeGreen");

$("td:contains('Prog')").css("background-color", "LimeGreen");

$("td:contains('Math.')").css("background-color", "DodgerBlue");

$("td:contains('LV2')").css("background-color", "Teal");

$("td:contains('Log')").css("background-color", "Gray");

$("td:contains('Auto')").css("background-color", "Orange");

$("td:contains('Sport')").css("background-color", "HotPink");

$("td:contains('ETech')").css("background-color", "OrangeRed");

$("td:contains('Ang')").css("background-color", "Sienna");

$("table:visible")

$("html").css("height","100%");



});
