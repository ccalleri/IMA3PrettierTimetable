# IMA3PrettierTimetable

Some CSS to make IMA3 Timetable more eye-friendly
Right now I use the  Add On Tampermonkey (https://bit.ly/JqtdAe) with a Jquery script to color specific "td" based on their content.
I temporary use Stylus (https://bit.ly/2kyIJ16e) for the general CSS, but it should be merger into the Jquery script.

.
![alt text](https://gitlab.utc.fr/ccalleri/IMA3PrettierTimetable/raw/master/Capture.PNG)